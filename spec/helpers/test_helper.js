var f = function () {
  return 'I am a custom Test Helper';
};

// This works, but then foo is accessible only via "this"
(function() {
  beforeEach(function() {
    this.foo = f;
  });
})();

// This doesn't work (jasmineRequire is undefined...)
// http://jasmine.github.io/edge/custom_boot.html
// (function() {
//   var jasmineInterface = jasmineRequire.interface(jasmine, jasmine.getEnv());
//   jasmineInterface.foo = f;
// })();

// This works, but then foo is accessible only via jasmine.getEnv()
(function() {
  jasmine.getEnv().foo = f;
})();

// This works, but it's not very good
(function() {
  global.foo = f;
})();

