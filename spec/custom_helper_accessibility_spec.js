describe('A custoom helper', function () {
  it('is accessible via this', function () {
    expect(typeof this.foo).toBe('function');
    expect(this.foo()).toEqual('I am a custom Test Helper');
  });

  it('is accessible via jasmine.getEnv()', function () {
    expect(typeof jasmine.getEnv().foo).toBe('function');
    expect(jasmine.getEnv().foo()).toEqual('I am a custom Test Helper');
  });

  it('is accessible globally', function () {
    expect(typeof foo).toBe('function');
    expect(foo()).toEqual('I am a custom Test Helper');
  });
});
